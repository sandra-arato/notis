// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(
  function($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })

    .state('app.logo', {
      url: '/logo',  
      views: {
        'menuContent': {
          templateUrl: 'templates/logo.html',
          controller: 'LogoCtrl'
        }
      }
    })
    .state('app.login', {
      url: '/login',  
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })

    .state('app.hello', {
      url: '/hello',  
      views: {
        'menuContent': {
          templateUrl: 'templates/hello.html'
        }
      }
    })

    .state('app.new-trip', {
      url: '/new-trip',
      views: {
        'menuContent': {
          templateUrl: 'templates/new-trip.html',
          controller: 'AddTripCtrl'
        }
      }
    })
    .state('app.trips', {
      url: '/trips',
      views: {
        'menuContent': {
          templateUrl: 'templates/trips.html',
          controller: 'TripsCtrl'
        }
      }
    })
    .state('app.trip', {
        url: "/trip/:id",
        views: {
        'menuContent': {
          templateUrl: 'templates/trip.html',
          controller: 'CurrentTripCtrl'
        }
      }
    })
    .state('app.alt', {
      url: '/alt/:opt',  
      views: {
        'menuContent': {
          templateUrl: 'templates/alternative.html',
          controller: 'AltCtrl'
        }
      }
    });


});
