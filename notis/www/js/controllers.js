var app = angular.module('starter.controllers', ['googleplus', 'ionic']);

app.config(['GooglePlusProvider', function(GooglePlusProvider) {
     GooglePlusProvider.init({
        clientId: '91043627044-9ttq46kogbm1g6m7rr2jgiimink3b6ap.apps.googleusercontent.com',
        apiKey: 'AIzaSyCw7S1J8iQzV_kwIPFVhtN4c2b19raHe7o'
     });
}]);

app
    .controller('AppCtrl', function($scope, $location, $window) {
        // Form data for the login modal
        $scope.user;

        if($window.localStorage.googleAccount) {
           $scope.user = JSON.parse($window.localStorage.googleAccount);
            console.log($scope.user);

        }

    })
    .controller('LoginCtrl', ['$scope', '$http', '$window', 'GooglePlus', function ($scope, $http, $window, GooglePlus) {
        $scope.login = function () {
            console.log('hello world');

            if ($window.localStorage.tripData || $window.localStorage.savedTrips) {
                $window.localStorage.tripData = '';
                $window.localStorage.savedTrips = '';
            }

            GooglePlus.login().then(function (authResult) {
                $window.localStorage.googleAccessToken = authResult.access_token;
                console.log(authResult);

                GooglePlus.getUser().then(function (user) {
                    console.log(user);
                    $window.localStorage.googleAccount = JSON.stringify(user);
                    $window.location.href = '/#/app/trips';
                });

                $http({
                    method: 'GET',
                    url: 'http://localhost:8080/auth?token=' + authResult.access_token
                }).success(function(d) {
                    $window.localStorage.tripData = JSON.stringify(d);

                });

            }, function (err) {
                console.log(err);
            });
        };
        $scope.skip = function () {
            if($window.localStorage.savedTrips) {
                $window.localStorage.savedTrips = '';
            }
            $window.location.href = '#/app/trips';
        };
    }])

    .controller('TripsCtrl', function($scope, $location, $window) {
        // Form data for the login modal

        var savedTrips;
        $scope.items = [];



        if($window.localStorage.savedTrips && $window.localStorage.savedTrips.length > 0) {
            savedTrips = JSON.parse($window.localStorage.savedTrips);
            // savedTrips = JSON.parse(savedTrips[0]);
            for (var i = 0; i < savedTrips.length; i++) {
                savedTrips[i] = JSON.parse(savedTrips[i]);
            };
            console.log('These are my saved trips:');
            console.log(savedTrips);
            $scope.items = savedTrips;
        }
    })

    .controller('CurrentTripCtrl', function($scope, $location, $window, $http) {
        // Form data for the login modal

        var id = $location.url().split('/')[3],
            savedTrips;

        if($window.localStorage.savedTrips && $window.localStorage.savedTrips.length > 0) {
            savedTrips = JSON.parse($window.localStorage.savedTrips);
            // savedTrips = JSON.parse(savedTrips[0]);

            for (var i = 0; i < savedTrips.length; i++) {
                savedTrips[i] = JSON.parse(savedTrips[i]);
            };
            console.log('These are my saved trips:');
            console.log(savedTrips[id]);
        }

        if(savedTrips) {
             $scope.trip = savedTrips[id];
        }
       
        $scope.getAlt = function (e) {
            var stageId = $(e.currentTarget).data('stage'),
                stageIndex = $(e.currentTarget).data('index'),
                alternatives = [];
            $window.localStorage.routeHistory = $window.location.href;
            // "9605bc6c-b1c2-437d-bae6-c6d545885575"
            $http.get('http://localhost:8080/trip/alternatives/?stageId=' + stageId).
              success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                console.log('success', data);
                console.log('what is happening?', data);
                // remap the data for the template
                if(data[0].flightInfo) {
                   console.log('flight alternatives updated');
                   // $window.localStorage.savedTrips = JSON.stringify(savedTrips[0]);

                   var results = data[0].flightInfo.results,
                    len = results.length;
                   console.log(data[0].flightInfo.results);
                    for(var i = 0; i < len; i++) {
                        console.log(results[i]);
                        var description =  'Departs:' + results[i].itineraries[0].outbound.flights[0].departs_at.split('T')[1];
                        var option = {
                            name: results[i].itineraries[0].outbound.flights[0].operating_airline + ' ' + results[i].itineraries[0].outbound.flights[0].flight_number,
                            description: description,
                            price: '£' + results[i].fare.total_price
                        }

                        alternatives.push(option);
                    }
                    console.log(alternatives);
                    $window.localStorage.alternatives = JSON.stringify(alternatives);
                   $window.location.href = '#/app/alt/' + stageIndex;
                } else if (data[0].taxiInfo) {

                    console.log('taxi alternatives updated');
                   // $window.localStorage.savedTrips = JSON.stringify(savedTrips[0]);
                   var results = data[0].taxiInfo,
                    len = results.length;
                   
                    for(var i = 0; i < 2; i++) {
                        console.log(results[i]);

                        var option = {
                            name: results[i].companyName,
                            description: 'Phone: ' + results[i].telNumber,
                            price: '£' + results[i].farePrice
                        }

                        alternatives.push(option);
                    }
                    console.log('what is happening?');
                    console.log(alternatives);
                    $window.localStorage.alternatives = JSON.stringify(alternatives);
                   $window.location.href = '#/app/alt/' + stageIndex;
                   console.log('what is happening?');
                }
              }).
              error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log('error in 3rd get request', data);
              });

            /////////////////////////////////////////////////////
            //
            // This is giving me 405, disabled till it gets fixed
            //
            ////////////////////////////////////////////////////

            $http.get('http://localhost:8080/service/allianz').
              success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                console.log('success with allianz', data);
                console.log(data);
                $window.localStorage.allianz = JSON.stringify(data);
              }).
              error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log('error in allianz get request', data);
              });

        }




    })

    .controller('AddTripCtrl', function($scope, $location, $window, $http, $cordovaGeolocation) {
        var localData = {},
            savedTrips;
        $scope.flight = '';
        $scope.destination = '';
        $scope.results = [];


        if($window.localStorage.savedTrips) {
            savedTrips = JSON.parse($window.localStorage.savedTrips);
        } else {
            savedTrips = [];
        }

        //preloeading flight options
        if ($window.localStorage.tripData) {
            localData = JSON.parse($window.localStorage.tripData);
            $scope.items = localData;
        }

        
        // getting origin via geoloc
        // var posOptions = {
        //     timeout: 10000,
        //     enableHighAccuracy: false
        // };
        
        // $cordovaGeolocation
        // .getCurrentPosition(posOptions)
        // .then(function(position) {
        //     console.log(position);
        //     localData.origin = position.coords;
        //     var reverseGeocoder = new google.maps.Geocoder();
        //     reverseGeocoder.geocode( { 'latLng':{lat: position.coords.latitude, lng: position.coords.longitude}}, function(results, status) {
        //         if (status == google.maps.GeocoderStatus.OK) {
        //           console.log(results);
        //           $('#origin').val(results[0].formatted_address);
        //           localData.origin.address = results[0].formatted_address;
        //         } else {
        //           console.log('Google Geocoding has failed: ' + status);
        //         }
        //       });

        // }, function(err) {
        //     // error
        // });

        $scope.getFlight = function(e) {
            // console.log(e.currentTarget);
            var flight = $('#flightChooser').val().split('(')[1],
                len = flight.length,
                date = flight.slice(0, len - 1).split(' ');
                bool = true;
            
            for(var i =0; i < 2; i++) {
                if(localData[i].departureTime.month == date[0] && localData[i].departureTime.dayOfMonth.toString() == date[1]) {
                    console.log('yaya');
                    break;
                }
            }
            // console.log(localData, localData[i].uuid);
            localData.calendarEntryUUID = localData[i].uuid;
            localData.flightIndex = i;
        }


        // setting destination w google autocomplete

        $scope.searchDest = function() {
            var address = $('#destination').val();
            
            var service = new google.maps.places.AutocompleteService();
            if(address.length > 0) {
                service.getQueryPredictions({
                    input: address
                }, function(predictions, status) {
                    if (status != google.maps.places.PlacesServiceStatus.OK) {
                        console.log(status);
                        return;
                    } else {
                        
                        if ( predictions.length > 0) {
                            $scope.results = predictions;
                        }
                        
                    }
                });
            }
            

            var geocoder = new google.maps.Geocoder(),
                address = $('#destination').val();

            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  console.log(results[0].geometry.location);
                  localData.destination =  {
                    address: address,
                    latitude: results[0].geometry.location.A,
                    longitude: results[0].geometry.location.F
                  }
                } else {
                  console.log('Google Geocoding has failed: ' + status);
                }
            });
        }

        $scope.searchOr = function() {
            var address = $('#origin').val();
            
            var service = new google.maps.places.AutocompleteService();
            if(address.length > 0) {
                service.getQueryPredictions({
                    input: address
                }, function(predictions, status) {
                    if (status != google.maps.places.PlacesServiceStatus.OK) {
                        console.log(status);
                        return;
                    } else {
                        
                        if ( predictions.length > 0) {
                            $scope.results = predictions;
                        }
                        
                    }
                });
            }
            

            var geocoder = new google.maps.Geocoder(),
                address = $('#origin').val();

            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  console.log(results[0].geometry.location);
                  localData.origin =  {
                    address: address,
                    latitude: results[0].geometry.location.A,
                    longitude: results[0].geometry.location.F
                  }
                } else {
                  console.log('Google Geocoding has failed: ' + status);
                }
            });
        }

        $scope.submit = function() {
            if(localData.destination && localData.origin) {
                console.log(localData);
                var url = 'http://localhost:8080/trip?calendarEntryUUID=';
                url = url + localData.calendarEntryUUID + '&';
                url = url + 'originLat=' + localData.origin.latitude + '&';
                url = url + 'originLong=' + localData.origin.longitude + '&';
                url = url + 'destinationLat=' + localData.destination.latitude + '&';
                url = url + 'destinationLong=' + localData.destination.longitude;

                $http.get(url).
                  success(function(data, status, headers, config) {
                    // this callback will be called asynchronously
                    // when the response is available
        
                    console.log('success in journey request', data);
                    data[0].location = localData.origin;
                    data[2].location = localData.destination;
                    data[1].flight = localData[localData.flightIndex];
                    data[1].flight.statusOk = data[0].statusOk && data[1].statusOk && data[2].statusOk;

                    savedTrips.push(JSON.stringify(data));

                    $window.localStorage.savedTrips =  JSON.stringify(savedTrips);
                  }).
                  error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    console.log('error in 2nd get request', data);
                  });
            }
            
        }


    })
    .controller('LogoCtrl', function($scope, $window) {
        // Form data for the login modal
        
        // setTimeout(function(){
        //     $window.location.href = "#/app/login"
        // }, 3000);
    })

    .controller('AltCtrl', function($scope, $window, $location) {

        $scope.back = '#/app/trips';
        $scope.alternatives;
        $scope.allianz;


        if($window.localStorage.routeHistory) {
            $scope.back = $window.localStorage.routeHistory;
        }

         if($window.localStorage.allianz) {
            console.log($window.localStorage.allianz);
            $scope.allianz = JSON.parse($window.localStorage.allianz);
           console.log($scope.allianz);
        }
        

        var optionMap = [$location.url().split('/')[3],$scope.back.split('/')[6]];
        $('#allianz').children().addClass('hidden');
        $scope.tab = function (e) {
            
            var index = $(e.currentTarget).index();
            if(index === 0 ){

                $('#first').addClass('first').removeClass('second third');
                $('#second').addClass('second').removeClass('first third');
                $('#third').addClass('third').removeClass('first second');

                $('[data-index="0"]').removeClass('hidden');
                $('[data-index="1"]').addClass('hidden');
                $('#allianz').children().addClass('hidden');
            } else if(index === 1) {

                $('#first').addClass('second').removeClass('first third');
                $('#second').addClass('first').removeClass('second third');
                $('#third').addClass('second').removeClass('first third');

                $('[data-index="0"]').addClass('hidden');
                $('[data-index="1"]').removeClass('hidden');
                $('#allianz').children().addClass('hidden');
            } else {

                $('#first').addClass('third').removeClass('first second');
                $('#second').addClass('second').removeClass('first third');
                $('#third').addClass('first').removeClass('third second');

                $('[data-index="0"]').addClass('hidden');
                $('[data-index="1"]').addClass('hidden');
                $('#allianz').children().removeClass('hidden');
            }
        }
        // pushing alternatives to the page of the given risk
        if($window.localStorage.alternatives && $window.localStorage.alternatives.length > 0) {
            $scope.alternatives = JSON.parse($window.localStorage.alternatives);
        } else {
            // $('#second').hide();
            // $('#third').hide();
            $scope.alternatives = [{name: 'You\'re in Trouble! ', description: 'Our system hasn\'t found a possible solution for you unfortunately. Luckily, Allianz is always there when you need them, simply call them with the Assistance button below.', price: 'Free'}];
        }
    });

